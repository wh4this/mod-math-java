package langModel;


/**
 * Class MyNaiveLanguageModel: class implementing the interface LanguageModel by creating a naive language model,
 * i.e. a n-gram language model with no smoothing.
 * 
 * @author faivre habib
 *
 */
public class MyNaiveLanguageModel implements LanguageModel {
	/**
	 * The NgramCounts corresponding to the language model.
	 */
	protected NgramCounts ngramCounts;
	
	/**
	 * The vocabulary of the language model.
	 */
	protected Vocabulary vocabulary;
	
	
	/**
	 * Constructor.
	 */
	public MyNaiveLanguageModel(){
		this.vocabulary = new MyVocabulary();
	}
	

	@Override
	public void setNgramCounts(NgramCounts ngramCounts) {
		this.ngramCounts = ngramCounts;
		this.vocabulary.scanNgramSet(ngramCounts.getNgrams());
	}

	@Override
	public int getLMOrder() {
		return this.ngramCounts.getMaximalOrder();
	}

	@Override
	public int getVocabularySize() {
		return this.vocabulary.getSize();
	}

	@Override
	public Double getNgramProb(String ngram) {
		int ngramSize = ngram.split(" ").length;
		double sum = 0.0;
		String historique;
		double ngramProb = this.ngramCounts.getCounts(ngram);
		if (ngramSize == 1) {
			return ngramProb / this.ngramCounts.getTotalWordNumber();
		} else if (ngramSize < this.getLMOrder()) {
				historique = NgramUtil.getHistory(ngram, ngramSize);
			} else {
				historique = NgramUtil.getHistory(ngram, this.getLMOrder());
		}
		for (int i = 0; i < this.vocabulary.getWords().size(); i++) {
				sum += this.ngramCounts.getCounts(historique);
		}
		if (sum == 0.0) {
			return 0.0;
		}
		return ngramProb / sum;
	}

	@Override
	public Double getSentenceProb(String sentence) {
		double sum = 1.0;
		for (String ngram : NgramUtil.decomposeIntoNgrams(sentence, this.getLMOrder())) {
			sum = sum * this.getNgramProb(ngram);
		}
		return sum;
	}
}
