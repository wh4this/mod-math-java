package langModel;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


/**
 * Class MyNgramCounts: class implementing the interface NgramCounts. 
 * 
 * @author N. Hernandez and S. Quiniou (2015)
 *
 */
public class MyNgramCounts implements NgramCounts {
	/**
	 * The maximal order of the n-gram counts.
	 */
	protected int order;

	/**
	 * The map containing the counts of each n-gram.
	 */
	protected Map<String,Integer> ngramCounts;

	/**
	 * The total number of words in the corpus.
	 * In practice, the total number of words will be increased when parsing a corpus 
	 * or when parsing a NgramCounts file (only if the ngram encountered is a unigram one).
	 */
	protected int nbWordsTotal;
	
	
	/**
	 * Constructor.
	 */
	public MyNgramCounts(){
		ngramCounts = new HashMap<String,Integer>();
	}


	/**
	 * Setter of the maximal order of the ngrams considered.
	 * 
	 * In practice, the method will be called when parsing the training corpus, 
	 * or when parsing the NgramCounts file (using the maximal n-gram length encountered).
	 * 
	 * @param order the maximal order of n-grams considered.
	 */
	private void setMaximalOrder (int order) {
		this.order = order;
	}

	
	@Override
	public int getMaximalOrder() {
		return this.order;
	}

	
	@Override
	public int getNgramCounterSize() {
		return this.ngramCounts.size();
	}

	
	@Override
	public int getTotalWordNumber(){
		return this.nbWordsTotal;
	}
	
	
	@Override
	public Set<String> getNgrams() {
		return ngramCounts.keySet();
	}

	
	@Override
	public int getCounts(String ngram) {
		if (ngramCounts.get(ngram) == null) {
			return 0;
		}
		return ngramCounts.get(ngram);
	}
	

	@Override
	public void incCounts(String ngram) {
		if (ngramCounts.containsKey(ngram)) {
			ngramCounts.put(ngram, ngramCounts.get(ngram) + 1 );
		} else {
			ngramCounts.put(ngram, 1);
		}
	}

	
	@Override
	public void setCounts(String ngram, int counts) {
		ngramCounts.put(ngram, counts);
	}


	@Override
	public void scanTextString(String text, int maximalOrder) {
		for (String ngram : NgramUtil.generateNgrams(text, 1, maximalOrder)) {
			ngram = ngram.toLowerCase();
			incCounts(ngram);
		}
	}

	
	@Override
	public void scanTextFile(String filePath, int maximalOrder) {
		File file  = new File(filePath);
		Scanner sc;
		this.setMaximalOrder(maximalOrder);
		this.nbWordsTotal = 0;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				this.scanTextString(line, maximalOrder);
				this.nbWordsTotal += line.split(" ").length;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void writeNgramCountFile(String filePath) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(filePath, "UTF-8");
			for (String ngram : getNgrams()) {
				writer.println(ngram + "\t" + getCounts(ngram));
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void readNgramCountsFile(String filePath) {
		int maxOrder = 0;
		int wordNumber = 0;
		File file  = new File(filePath);
		Scanner sc;
			try {
				sc = new Scanner(file);
				while (sc.hasNextLine()) {
					String line = sc.nextLine();
					String[] ngramPart =  line.split("\t");
					this.setCounts(ngramPart[0], Integer.parseInt(ngramPart[1]));
					
					int ngramOrder = ngramPart[0].split(" ").length;
					if (maxOrder < ngramOrder) { maxOrder = ngramOrder; }
					if (ngramOrder == 1) { wordNumber += Integer.parseInt(ngramPart[1]); }
				}
				this.setMaximalOrder(maxOrder);
				this.nbWordsTotal = wordNumber;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	}

}
