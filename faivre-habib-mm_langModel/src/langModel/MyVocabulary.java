package langModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;


/**
 * Class MyVocabulary: class implementing the interface Vocabulary.
 * 
 * @author faivre habib
 *
 */
public class MyVocabulary implements Vocabulary {
	/**
	 * The set of words corresponding to the vocabulary.
	 */
	protected Set<String> vocabulary;
	
	/**
	 * Constructor.
	 */
	public MyVocabulary() {
		this.vocabulary = new TreeSet<String>();
	}
	
	@Override
	public int getSize() {
		return this.vocabulary.size();
	}

	@Override
	public Set<String> getWords() {
		return this.vocabulary;
	}

	@Override
	public boolean contains(String word) {
		word = word.toLowerCase();
		return this.vocabulary.contains(word);
	}

	@Override
	public void addWord(String word) {
		word = word.toLowerCase();
		this.vocabulary.add(word);
	}

	@Override
	public void removeWord(String word) {
		word = word.toLowerCase();
		this.vocabulary.remove(word);
	}

	@Override
	public void scanNgramSet(Set<String> ngramSet) {
		for (String ngram : ngramSet) {
			for (String word : ngram.split(" ")) {
				if (!this.contains(word)) {
					this.addWord(word);
				}
			}
		}
	}

	@Override
	public void readVocabularyFile(String filePath){
		File file  = new File(filePath);
		Scanner sc;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				String word = sc.nextLine();
				this.addWord(word);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public void writeVocabularyFile(String filePath) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(filePath, "UTF-8");
				for (String  word : getWords()) {
					writer.println(word);
				}
				writer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	}

}
