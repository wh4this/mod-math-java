package langModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class MyVocabularyTest {
	MyVocabulary vocabulary;
	MyNgramCounts ngramcounts;
	
	@Before
	public void setUp(){
		ngramcounts = new MyNgramCounts();
		ngramcounts.readNgramCountsFile("lm/bigram-100-train-en.lm");
		vocabulary = new MyVocabulary();
	}
	
	@Test
	public void testaddWord() {
		vocabulary.addWord("Allotest");
		assertEquals(vocabulary.contains("Allotest"), true);
		assertEquals(vocabulary.contains("ALLOTEST"), true);
	}

	@Test
	public void testremoveWord() {
		vocabulary.removeWord("Allotest");
		assertEquals(!(vocabulary.contains("Allotest")), true);
	}
	
	@Test
	public void testwriteVocabularyFile() {
		vocabulary.scanNgramSet(ngramcounts.getNgrams());
		vocabulary.writeVocabularyFile("tmp/en-sample.vocab");
	}

	@Test
	public void testscanNgramSet() {
		vocabulary.scanNgramSet(ngramcounts.getNgrams());
		assertTrue(vocabulary.contains("tree"));
	}
	
	@Test
	public void testreadVocabularyFile() {
		vocabulary.readVocabularyFile("tmp/en-sample.vocab");
		assertTrue(vocabulary.contains("tree"));
	}
	
	@Rule
	public TestName name = new TestName();

	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}
}

