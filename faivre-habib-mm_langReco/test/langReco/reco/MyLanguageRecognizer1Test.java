package langReco.reco;

import langReco.eval.Performance;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class MyLanguageRecognizer1Test {
	
	@Test
	public void testMyLanguageRecognizer1Unigram() {
		String goldSentPath = "data/gold/gold-sent.txt";
		String goldLangPath = "data/gold/gold-lang.txt";
		String configFile = "lm/fichConfig_unigram-100.txt";
		LanguageRecognizer myRecognizer1 = new MyLanguageRecognizer1(configFile);
		String hypLangFilePath = "tmp/hypUni";
		
		myRecognizer1.recognizeFileLanguage(goldSentPath, hypLangFilePath);
		System.out.printf("System performance = %f\n", Performance.evaluate(goldLangPath, hypLangFilePath));
	}
	
	@Test
	public void testMyLanguageRecognizer1Bigram() {
		String goldSentPath = "data/gold/gold-sent.txt";
		String goldLangPath = "data/gold/gold-lang.txt";
		String configFile = "lm/fichConfig_bigram-100.txt";
		LanguageRecognizer myRecognizer1 = new MyLanguageRecognizer1(configFile);
		String hypLangFilePath = "tmp/hypBi";
		
		myRecognizer1.recognizeFileLanguage(goldSentPath, hypLangFilePath);
		System.out.printf("System performance = %f\n", Performance.evaluate(goldLangPath, hypLangFilePath));
	}
	
	@Test
	public void testMyLanguageRecognizer1Trigram() {
		String goldSentPath = "data/gold/gold-sent.txt";
		String goldLangPath = "data/gold/gold-lang.txt";
		String configFile = "lm/fichConfig_trigram-100.txt";
		LanguageRecognizer myRecognizer1 = new MyLanguageRecognizer1(configFile);
		String hypLangFilePath = "tmp/hypTri";
		
		myRecognizer1.recognizeFileLanguage(goldSentPath, hypLangFilePath);
		System.out.printf("System performance = %f\n", Performance.evaluate(goldLangPath, hypLangFilePath));
	}
	
	@Rule
	public TestName name = new TestName();

	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}
}
